#!/bin/bash

MAIN_SCRIPTS=~itopols2/genomics
ORIGDIR='/data2/unige/fig/UHTS/GAII'

. $MAIN_SCRIPTS/rta_api.sh


QUEUE=long

declare -a DIRS
DIRS=( $(find -P "${ORIGDIR}/" -maxdepth 1 -type d -regextype posix-extended -regex "(.*/)?${RUNRX}" -printf '%f\n' | sort -r) )

cat << BSUB_END
#!/bin/bash

#BSUB -L /bin/bash
#BSUB -R "span[hosts=1]"
#BSUB -R "rusage[mem=256]"
#BSUB -M 262144
#BSUB -n 1
#BSUB -q ${QUEUE}
#BSUB -J "checkdirs[1-${#DIRS[@]}]"
#BSUB -u bioinfogenomics@unige.ch
#BSUB -N
#BSUB -W 100:59

DIRS=( '' ${DIRS[@]} )

DIR="\${DIRS[\$LSB_JOBINDEX]}"

# check if dir name is ok
if [ -z "\${DIR}" ]; then
	echo "wrong job index \$LSB_JOBINDEX"
	exit 1;
fi
if [ ! -d "${BASEDIR}/\${DIR}" ]; then
	echo "${BASEDIR}/\${DIR} : bad directory"
	exit 1;
fi

# test and report errors
ERROR=0
for Z in \$(find "${BASEDIR}/\${DIR}" -type f -name '*.gz'); do
	if gunzip --test "\${Z}"; then
		:
	else
		echo "\${Z} : error"
		(( ERROR++ ))
	fi
done
if (( ERROR > 0 )); then
	echo "There were errors in ${BASEDIR}/\${DIR}"
	exit 1;
else
	echo "${BASEDIR}/\${DIR} : Everything okay !"
	exit 0;
fi


BSUB_END
