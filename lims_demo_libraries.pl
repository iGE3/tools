#!/usr/bin/env perl

use strict;
use Encode;

use Data::Dumper;

$/ = "\r\n";

my $basedir = '/data/unige/fig/UHTS/rundata';

open my $c, '<:encoding(UTF-8)', 'people.csv'
	or die 'cannot open csv';
open my $f, '>:encoding(UTF-8)', 'files.csv'
	or die 'cannot output csv';

while (<$c>) {
	my %line;

	chomp;

	my $saveline = $_;
	$_ =~ s{"}{}g;
	@line{'first_name','run_folder','lane_nb','code_name','short_name','name','sequence3','sequence5','output_dir','pipeline_version_id'} = split /;/;

	# remove non-word
	$line{'name'} =~ s/\W//g;

	my $pipecmd;

	if ($line{'pipeline_version_id'} == 10) {
		my $tag = ($line{'sequence3'} ne '') ? $line{'sequence3'} : "NoIndex";
		$pipecmd = "find '${basedir}/$line{'run_folder'}/$line{'output_dir'}/Project_$line{'code_name'}/Sample_$line{'name'}/' -type f -iregex '.*/$line{'name'}_*${tag}_L00$line{'lane_nb'}_R[0-9]_[0-9]+.fastq[^/]*'  -printf '\"\%f\";\"\%s\"\\r\\n'";
	} elsif ($line{'pipeline_version_id'} == 12) {
		$pipecmd = "find '${basedir}/$line{'run_folder'}/$line{'output_dir'}/$line{'code_name'}/' -type f -iregex '.*/$line{'name'}_S[0-9]+_L00$line{'lane_nb'}_R[0-9]_[0-9]+.fastq[^/]*' -printf '\"\%f\";\"\%s\"\\r\\n'";
	} else {
		die "unknown type $line{'pipeline_version_id'}"
	}

	open my $p, '-|:encoding(UTF-8)', $pipecmd
		or die "Failed to open pipe:\n$pipecmd\n";

	while (<$p>) {
		print $f "${saveline};$_";
	}
	close $p;
}
close $c;

exit 0;
