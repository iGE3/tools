#!/bin/bash

#BSUB -L /bin/bash
#BSUB -R "span[hosts=1]"
#BSUB -R "rusage[mem=1000]"
#BSUB -M 1024000
#BSUB -n 1
#BSUB -q uhts
#BSUB -J 'move_it'
#BSUB -u bioinfogenomics@unige.ch
#BSUB -N
#BSUB -W 100:59


# used for RX (reg ex to match run directories)
MAIN_SCRIPTS=~itopols2/genomics
. $MAIN_SCRIPTS/rta_api.sh

# over write directories (we might be using temporarily different directories during the copy procedure)
BASEDIR='/data/unige/fig/UHTS/rundata/'
SRCDIR='/data2/unige/fig/UHTS/GAII/'
WORKDIR="${BASEDIR}move_from_data2/"

# get list of directories (either by generating them or reading todo list)
DIRS="$(cat "${WORKDIR}/rsync.todo.lst.txt")"
# DIRS="$(find -P "${SRCDIR}/" -maxdepth 1 -type d -regextype posix-extended -regex "(.*/)?${RUNRX}" -printf '%f\n' | sort -r)"
# echo "${DIRS}" | tee "${WORKDIR}/rsync.todo.lst.txt"
# exit 0;

# TODO extra filtering if needed

# make symlinks
echo '================================================================'
echo 'symlinks...'
echo '----------------------------------------------------------------'
for D in $DIRS; do
	if [ -e "${BASEDIR}/$D" ]; then
		echo "skip $D..."
	else
		ln -v -s "${ARCHIVEDIR}/${D}" "${BASEDIR}/$D"
	fi
done;
# exit 0;

# process directories
for D in $DIRS; do
	echo '================================================================'
	echo "moving $D..."
	echo '----------------------------------------------------------------'

	# only replace *those directories* that are temporarily symlinks (e.g.: to archive, or to source directory)
	if [ ! -L "${BASEDIR}/$D" ]; then
		echo 'Not a symlink, skip !'
		continue
	fi

	# check source
	if [ ! -d "${SRCDIR}/$D" ]; then
		echo 'No source dir, skip !'
		continue
	fi

	# do copy
	rsync -avP --append-verify --inplace "${SRCDIR}/$D" "${WORKDIR}/"

	# check if links is still there
	if [ ! -L "${BASEDIR}/$D" ]; then
		echo 'WTF no symlink ?!'
		continue
	fi

	# replace
	rm "${BASEDIR}/$D"
	mv "${WORKDIR}/$D" "${BASEDIR}/$D"
done
