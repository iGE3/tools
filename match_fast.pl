#!/usr/bin/env perl
use strict;

# 
# 1. Load key
#
my $keyfile = shift @ARGV;

defined $keyfile or die "Please give a key file !\n";

print STDERR "Load keys from $keyfile\n";
my @keys;
my $rx = qr/Locus_[[:digit:]]+_Transcript_[[:digit:]]+_[[:digit:]]+_/;;
{
	open my $f, "<", $keyfile
		or die "open error $keyfile";
	@keys = map { s{[\n\r]+$}{}g; $_} <$f>;
	close $f;

	print STDERR "$#keys keys in file\n";
	print STDERR "Build regex\n";
}
my %reverse = map { $_ => 1 } @keys;



#
# 2. Scan input for lines matching the keys
#
print STDERR "Scan input ${ARGV[0]} ...\n";
my $m = 0;
my $l = 0;
my $p = '';

my %matches;
while ( <> ) {
	if ( $_ =~ /Locus_[[:digit:]]+_Transcript_[[:digit:]]+_[[:digit:]]+_/ ) {
		next if (! exists $reverse { $& } );
		# .. do not print matching lines but store them into hash
		$matches{ $& } =  $_;
		++$m;
		print STDERR "\r${p}Line $l     Found $m / $#keys matches...";
	}
} continue {
	if (((++$l) & 127) == 0) {
		$p = int(tell() * 100 / (-s $ARGV))  . '% - '
			if( -e $ARGV);
		print STDERR "\r${p}Line $l";
	}	
}

#
# 3. for each key either printing matching line or 'NOT FOUND'
#
print STDERR "\nWrite results ...\n";
foreach (@keys) {
	print $_ . ":\t" . (exists $matches{$_} ? $matches{$_} : 'NOT FOUND!') . "\n";
}
print STDERR "Done\n";
