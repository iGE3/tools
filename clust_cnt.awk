#!/bin/awk -f
function printstats() {
	if (numentries) {
		print "Cluster " cluster ":\t" numentries " entries"
		totentries += numentries;
	}
}

/^>Cluster/ { 
	printstats();
	cluster = $2; numclust++; numentries = 0; 
	next; 
}
{ numentries++ }
END { 
	printstats(); 
	print "num clusters\t" numclust 
    print "total\t" totentries " entries"; 
}